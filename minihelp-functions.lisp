(DEFINE (
         (MEMBER (LAMBDA (A X)
                         (COND
                           ((NULL X) NIL)
                           ((EQUAL A (CAR X)) T)
                           (T (MEMBER A (CDR X)))) ))

         (CAAR (LAMBDA (X) (CAR (CAR X))) )
         (CAAAR (LAMBDA (X) (CAR (CAR (CAR X)))) )

         (CADR (LAMBDA (X) (CAR (CDR X))) )
         (CADDR (LAMBDA (X) (CAR (CDR (CDR X)))) )

         (CAADR (LAMBDA (X) (CAR (CAR (CDR X)))) )
         (CADAR (LAMBDA (X) (CAR (CDR (CAR X)))) )
         (CADDAR (LAMBDA (X) (CAR (CDR (CDR (CAR X))))) )
         (CADDDAR (LAMBDA (X) (CAR (CDR (CDR (CDR (CAR X)))))) )

         (CDAR (LAMBDA (X) (CDR (CAR X))) )
         (CDDR (LAMBDA (X) (CDR (CDR X))) )
         (CDAAR (LAMBDA (X) (CDR (CAR (CAR X)))) )

         (RETURN (LAMBDA (X) X))

         (NOT (LAMBDA (A)
                      (COND
                        ((EQ A NIL) T)
                        (T NIL))))
         (MAPCAR (LAMBDA (X FN)
                         (PROG (XS YS FUN)
                               (SETQ FUN FN)
                               (SETQ XS X)
                            L1 (COND ((NULL XS) (RETURN YS)))
                               (SETQ YS (APPEND YS (LIST (FN (CAR XS)))))
                               (SETQ XS (CDR XS))
                               (GO L1))))
         (MAPCARNUM (LAMBDA (X FN)
                         (PROG (XS YS FUN N)
                               (SETQ FUN FN)
                               (SETQ XS X)
                               (SETQ N 1)
                            L1 (COND ((NULL XS) (RETURN YS)))
                               (SETQ YS (APPEND YS (LIST (FN (CAR XS) N))))
                               (SETQ XS (CDR XS))
                               (SETQ N (ADD1 N))
                               (GO L1))))
))
