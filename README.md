# MINIHELP interpreter

Interpreter for a subset of the LISP 1.5 dialect HELP written in Common Lisp.
Written for the purpose of running
[Futamuras Partial Evaluator](https://fi.ftmr.info/PE-Museum/SelfApplicablePE.pdf)
from 1969.

## Requirements

`sbcl` [Steel Bank Common Lisp](http://www.sbcl.org/platform-table.html)

## Running MINIHELP programs

The command to run a MINIHELP programs is:

```
sbcl --script minihelpint.lisp <MINIHELP_PROGRAM>
```

There are some different programs in the `programs` directory.
The file `programs/minialgol/fpea_original.lisp` demonstrates the 2nd Futamura Projection by
running Futmaura's original experiments from the [SelfApplicablePE.pdf](https://fi.ftmr.info/PE-Museum/SelfApplicablePE.pdf) printout.

## Files

### `minihelpint.lisp`

 - The MINIHELP interpreter

### `minihelp-functions`

 - Contains definitions of auxiliary functions. Automatically loaded when running `minihelpint.lisp`
