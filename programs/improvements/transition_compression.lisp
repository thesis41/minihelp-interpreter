(DEFLIST ((PROG2 (LAMBDA (X A)
                         (COND ((EVAL (CAR X) A) (EVAL (CADR X) A))
                               (T (EVAL (CADR X) A)))))) FEXPR)
(DEFINE (
         (ALPHA1 (LAMBDA (P RL CL CVL)
                         (BT P RL CL CVL NIL NIL)))
         (BT (LAMBDA (P RL CL CVL A6)
                     (COND
                       ((NULL RL) (LIST (QUOTE LAMBDA) NIL
                                        (LIST (QUOTE QUOTE) (APPLY P CVL NIL))))
                       ((AND (ATOM P) (GET P (QUOTE EXPR)))
                        (BT (GET P (QUOTE EXPR)) RL CL CVL A6))
                       ((ATOM P) (BT (EVAL P NIL) RL CL CVL A6))
                       ((EQ (CAR P) (QUOTE LAMBDA))
                        (COND
                          ((NRM (CADDR P) RL) (BT05 (STK P CL A6) P RL CL CVL A6))
                          (T (BT1 (STK P CL A6) (GENSYM) P RL CL CVL A6)) )))))
         (BT05 (LAMBDA (X P RL CL CVL A6)
                      (COND
                        ((NULL X)
                         (BT3 (CADDR P) RL CL CVL A6))
                        ((NULL (ASSOC CVL (CDR X)))
                         (BT3 (CADDR P) RL CL CVL A6))
                        (T (CDR (ASSOC CVL (CDR X)))) )) )
         (BT1 (LAMBDA (X Y P RL CL CVL A6)
                      (COND
                        ((NULL X)
                         (CDLL Y
                               (LIST (QUOTE LAMBDA) RL (BT3 (CADDR P) RL CL CVL (CONS (CONS (CONS P CL)
                                                               (LIST (CONS CVL Y))) A6))) ))
                        ((NULL (ASSOC CVL (CDR X))) (CDLL
                                                     Y (LIST (QUOTE LAMBDA) RL (BT3 (CADDR P) RL CL CVL
                                                            (PROG2 (NCONC X (LIST (CONS CVL Y))) A6)))))
                        (T (CDR (ASSOC CVL (CDR X)))) )) )
         (NRM (LAMBDA (P RL) (COND
                               ((ATOM P) T)
                               ((EQ (CAR P) (QUOTE COND)) NIL)
                               ((EQ (CAR P) (QUOTE LAMBDA)) NIL)
                               (T (NRM1 (CDR P) RL)))))
         (NRM1 (LAMBDA (X RL)
                              (COND
                                ((NULL X) T)
                                ((NRM2 (CAR X) RL) (NRM1 (CDR X)))
                                (T NIL)
                                )))
         (NRM2 (LAMBDA (X RL)
                              (COND
                                ((ATOM X) T)
                                (T (NRTV X RL)))))
         (STK (LAMBDA (P CL A6)
                      (ASSOC (CONS P CL) A6) ))
         (NRTV  (LAMBDA (P RL)
                        (COND
                          ((ATOM P) (NOT (MEMBER P RL)))
                          ((NUMBERP P) T)
                          ((EQ (CAR P) (QUOTE QUOTE)) T)
                          ((EQ (CAR P) (QUOTE COND)) (NRTV1 (CDR P) RL))
                          ((EQ (CAR P) (QUOTE GENSYM)) NIL)
                          (T (NRTV2 (CDR P) RL)) )))
         (NRTV1 (LAMBDA (X RL) (COND
                                 ((NULL X) T)
                                 ((AND(NRTV (CAAR X) RL) (NRTV (CADAR X) RL))
                                  (NRTV1 (CDR X) RL))
                                 (T NIL) )))
         (NRTV2 (LAMBDA (X RL) (COND
                                 ((NULL X) T)
                                 ((NRTV (CAR X) RL) (NRTV2 (CDR X) RL))
                                 (T NIL) )))
         (BT3 (LAMBDA (P RL CL CVL A6) (COND
                                            ((NRTV P RL)
                                             (LIST (QUOTE QUOTE) (EVAL P (PAIRLIS CL CVL))) )
                                            ((MEMBER P RL) P)
                                            ((ATOM (CAR P))
                                             (COND
                                               ((OR
                                                  (EQ (CAR P) (QUOTE ASSOC))
                                                  (EQ (CAR P) (QUOTE RLIST2))
                                                  (EQ (CAR P) (QUOTE COPY1))
                                                  (EQ (CAR P) (QUOTE NRTV))
                                                  (EQ (CAR P) (QUOTE RTVL1))
                                                  (EQ (CAR P) (QUOTE CTVL1))
                                                  (EQ (CAR P) (QUOTE CLIST))
                                                  (EQ (CAR P) (QUOTE PAIRLIS))
                                                  (EQ (CAR P) (QUOTE PPAIR))
                                                  (EQ (CAR P) (QUOTE GOLIST))
                                                  (EQ (CAR P) (QUOTE STK))
                                                  (EQ (CAR P) (QUOTE PROG2))
                                                  (EQ (CAR P) (QUOTE GENSYM))
                                                  (EQ (CAR P) (QUOTE CDLL))
                                                  )
                                                (CONS (CAR P) (RLIST (CDR P) RL CL CVL A6)))
                                               ((GET (CAR P) (QUOTE EXPR))
                                                (BT3 (CONS (GET (CAR P) (QUOTE EXPR)) (CDR P))
                                                     RL CL CVL A6))
                                               ((EQ (CAR P) (QUOTE COND))
                                                (BT4 (CDR P) RL CL CVL A6))
                                               ((OR (GET (CAR P) (QUOTE SUBR)) (GET (CAR P) (QUOTE FSUBR))
                                                    )
                                                (CONS (CAR P) (RLIST (CDR P) RL CL CVL A6)))
                                               ( T (ERROR P)) ))
                                            ((EQ (CAAR P) (QUOTE LAMBDA))
                                             (CONS (SRCHSTK (CAR P) (CADAR P)
                                                            (CDR P)
                                                            RL CL CVL A6)
                                                   (RLIST (RLIST2 (CDR P) RL) RL CL CVL A6) ))
                                            )))
         (RLIST (LAMBDA (X RL CL CVL A6) (COND
                                             ((NULL X) NIL)
                                             (T (CONS (BT3 (CAR X) RL CL CVL A6)
                                                      (RLIST (CDR X) RL CL CVL A6))) )))
         (RLIST2 (LAMBDA (X RL) (COND
                                  ((NULL X) NIL)
                                  ((NRTV (CAR X) RL) (RLIST2 (CDR X) RL))
                                  (T (CONS (CAR X) (RLIST2 (CDR X) RL))) )))
         (SRCHSTK (LAMBDA (X Y Z RL CL CVL A6)
                          (BT X (RTVL1 Z Y RL) (CTVL1 Z Y RL)
                              (EVLIS (CLIST Z RL) (PAIRLIS CL CVL)) A6) ))
         (RTVL1 (LAMBDA (Z Y RL) (COND
                                   ((NULL Z) NIL)
                                   ((NRTV (CAR Z) RL) (RTVL1 (CDR Z) (CDR Y) RL))
                                   (T (CONS (CAR Y) (RTVL1 (CDR Z) (CDR Y) RL))) )))
         (CTVL1 (LAMBDA (Z Y RL) (COND
                                   ((NULL Z) NIL)
                                   ((NRTV (CAR Z) RL)
                                    (CONS (CAR Y) (CTVL1 (CDR Z) (CDR Y) RL)))
                                   (T (CTVL1 (CDR Z) (CDR Y) RL)) )))
         (CLIST (LAMBDA (Z RL) (COND
                                 ((NULL Z) NIL)
                                 ((NRTV (CAR Z) RL)
                                  (CONS (CAR Z) (CLIST (CDR Z) RL)))
                                 (T (CLIST (CDR Z) RL)) )))
         (BT4 (LAMBDA (X RL CL CVL A6) (COND
                                           ((NRTV (CAAR X) RL)
                                            (COND
                                              ((EVAL (CAAR X) (PAIRLIS CL CVL))
                                               (BT3 (CADAR X) RL CL CVL A6))
                                              (T (BT4 (CDR X) RL CL CVL A6)) ))
                                           (T (CONS (QUOTE COND) (COND1 X RL CL CVL A6))) )))
         (COND1 (LAMBDA (X RL CL CVL A6) (COND
                                             ((NULL (CDR X))
                                              (LIST (LIST (BT5 (CAAR X) RL CL CVL A6)
                                                          (BT5 (CADAR X) RL CL CVL A6))) )
                                             (T (CONS (LIST (BT5 (CAAR X) RL CL CVL A6)
                                                            (BT5 (CADAR X) RL CL CVL A6))
                                                      (COND1 (CDR X) RL CL CVL A6))) )))
         (BT5 (LAMBDA (X RL CL CVL A6) (COND
                                           ((NRTV X RL) (COND
                                                          ((ATOM X) (LIST (QUOTE QUOTE) (EVAL X (PAIRLIS
                                                                                                  CL CVL))))
                                                          ((EQ (CAR X) (QUOTE QUOTE)) X)
                                                          ((EQ (CAR X) (QUOTE COND))
                                                           (COND2 (CDR X) RL CL CVL A6))
                                                          (T (CONS (CAR X) (CEVLIS (CDR X) RL CL CVL A6))) ))
                                           ((EQ (CAR X) (QUOTE COND))
                                            (COND2 (CDR X) RL CL CVL A6))
                                           (T (BT3 X RL CL CVL A6)) )))
         (COND2 (LAMBDA (Y RL CL CVL A6) (COND
                                             ((NRTV (CAAR Y) RL) (COND
                                                                   ((EVAL (CAAR Y) (PAIRLIS CL CVL))
                                                                    (BT5 (CADAR Y) RL CL CVL A6))
                                                                   (T (COND2 (CDR Y) RL CL CVL A6)) ))
                                             (T (CONS (QUOTE COND) (COND1 Y RL CL CVL A6))) )))
         (CEVLIS (LAMBDA (Y RL CL CVL A6) (COND
                                              ((NULL Y) NIL)
                                              (T (CONS (BT5 (CAR Y) RL CL CVL A6)
                                                       (CEVLIS (CDR Y) RL CL CVL A6))) )))
         (PAIRLIS (LAMBDA (X Y) (COND
                                    ((NULL X) NIL)
                                    (T (CONS (CONS (CAR X) (CAR Y))
                                             (PAIRLIS (CDR X) (CDR Y)))) )))
         (CDLL (LAMBDA (X Y) (CAR (DEFINE (LIST
                                            (LIST X Y)))) ))
         (GENSYM (LAMBDA NIL
                         (PROG (U)
                               (SETQ U (CAR GENSYM))
                               (CSETQ GENSYM (CDR GENSYM))
                               (RETURN U)  )))
         (ASSOC (LAMBDA (X A) (COND ((NULL A) NIL) ((EQUAL (CAAR A) X) (CAR
                                                                         A)) (T (ASSOC X (CDR A))))))
         ))
(CSET GENSYM1
      (G1 G2 G3 G4 G5 G6 G7 G8 G9 G10 G11 G12 G13 G14 G15 G16 G17
          G18 G19 G20 G21 G22 G23 G24 G25 G26 G27 G28 G29 G30 G31
          G32 G33 G34 G35 G36 G37 G38 G39 G40 G41 G42 G43 G44 G45
          G46 G47 G48 G49 G50 G51 G52 G53 G54 G55 G56 G57 G58 G59
          G60 G61 G62 G63 G64 G65 G66 G67 G68 G69 G70 G71 G72 G73
          G74 G75 G76 G77 G78 G79 G80 G81 G82 G83 G84 G85 G86 G87
          G88 G89 G90 G91 G92 G93 G94 G95 G96 G97 G98 G99 G100 G101
          G102 G103 G104 G105 G106 G107 G108 G109 G110 G111 G112
          G113 G114 G115 G116 G117 G118 G119 G120 G121 G122 G123
          G124 G125 G126 G127 G128 G129 G130 G131 G132 G133 G134
          G135 G136 G137 G138 G139 G140 G141 G142 G143 G144 G145
          G146 G147 G148 G149 G150 G151 G152 G153 G154 G155 G156
          G157 G158 G159 G160 G161 G162 G163 G164 G165 G166 G167
          G168 G169 G170 G171 G172 G173 G174 G175 G176 G177 G178
          G179 G180 G181 G182 G183 G184 G185 G186 G187 G188 G189
          G190 G191 G192 G193 G194 G195 G196 G197 G198 G199 G200
          G201 G202 G203 G204 G205 G206 G207 G208 G209 G210
          G211 G212 G213 G214 G215 G216 G217 G218 G219 G220
          G221 G222 G223 G224 G225 G226 G227 G228 G229 G230
          G231 G232 G233 G234 G235 G236 G237 G238 G239 G240
          G241 G242 G243 G244 G245 G246 G247 G248 G249 G250
          G251 G252 G253 G254 G255 G256 G257 G258 G259 G260
          G261 G262 G263 G264 G265 G266 G267 G268 G269 G270
          G271 G272 G273 G274 G275 G276 G277 G278 G279 G280
          G281 G282 G283 G284 G285 G286 G287 G288 G289 G290
          G291 G292 G293 G294 G295 G296 G297 G298 G299 G300
          G301 G302 G303 G304 G305 G306 G307 G308 G309 G310
          G311 G312 G313 G314 G315 G316 G317 G318 G319 G320
          G321 G322 G323 G324 G325 G326 G327 G328 G329 G330
          G331 G332 G333 G334 G335 G336 G337 G338 G339 G340
          G341 G342 G343 G344 G345 G346 G347 G348 G349 G350
          G351 G352 G353 G354 G355 G356 G357 G358 G359 G360
          G361 G362 G363 G364 G365 G366 G367 G368 G369 G370
          G371 G372 G373 G374 G375 G376 G377 G378 G379 G380
          G381 G382 G383 G384 G385 G386 G387 G388 G389 G390
          G391 G392 G393 G394 G395 G396 G397 G398 G399 G400
          ))
(CSETQ GENSYM GENSYM1)

(DEFINE (
         (TMINT (LAMBDA (Q R)
                        (TMEVAL Q NIL R Q)))
         (TMEVAL (LAMBDA (Q L R P)
                        (COND
                          ((NULL (CAR Q)) R)
                          ((EQ (CADAR Q) (QUOTE LEFT))
                           (TMEVAL (CDR Q) (CDR L) (CONS (CAR L) R) P))
                          ((EQ (CADAR Q) (QUOTE RIGHT))
                           (TMEVAL (CDR Q) (CONS (CAR R) L) (CDR R) P))
                          ((EQ (CADAR Q) (QUOTE WRITE))
                           (TMEVAL (CDR Q) L (CONS (CADDAR Q) (CDR R)) P))
                          ((EQ (CADAR Q) (QUOTE GOTO))
                           (TMEVAL (TMGO (CADDAR Q) P) L R P))
                          ((EQ (CADAR Q) (QUOTE IF))
                           (COND
                             ((EQ (CADDAR Q) (CAR R)) (TMEVAL (TMGO (CADDDAR Q) P) L R P))
                             (T (TMEVAL (CDR Q) L R P))))
                          (T (ERROR (QUOTE UNKNOWN-TURING-MACHINE-INSTRUCTION)))
                          ) ))
         (TMGO (LAMBDA (X P)
                            (COND
                              ((NULL P) (ERROR (QUOTE LABEL-NOT-FOUND)))
                              ((EQ X (CAAR P)) P)
                              (T (TMGO X (CDR P))) )))
                ))

(PROG (Q R TARGET TARGET1 COMP OBJ2)
      (SETQ Q (QUOTE ((0 IF 0 3)
                      (1 RIGHT)
                      (2 GOTO 0)
                      (3 WRITE 1))))
      (SETQ R (QUOTE (1 1 0 1 0 1)))
      (PRINT (QUOTE ***PROGRAM-1***))
      (PRINT Q)
      (PRINT (QUOTE ***TAPE***))
      (PRINT R)
      (PRINT (QUOTE ***VALUE-TMINT-1***))
      (PRINT (TMINT Q R))
      (CSETQ INDICES (CONS (CAR GENSYM) 'START))
      (CSETQ INDICES (APPEND (LIST (CONS (CAR GENSYM) 'TARGET-1)) (LIST INDICES)))
      (SETQ TARGET1 (ALPHA1 (QUOTE TMINT)
                           (QUOTE (R))
                           (QUOTE (Q))
                           (LIST Q)) )
      (SETQ TARGET (CAR TARGET1))
      (SETQ R (QUOTE (1 1 0 1 0 1)))
      (PRINT (QUOTE ***VALUE-TARGET-1***))
      (PRINT (APPLY TARGET (LIST R) NIL))

      (MAPCARNUM GENSYM1
        (FUNCTION (LAMBDA (U)
                          (PROG2
                            (COND
                              ((ASSOC U INDICES) (PRINTN (CDR (ASSOC U INDICES))))
                              (T ()))
                            (PROG2
                              (PRINTG U)
                              (PRINT (GET U (QUOTE EXPR))))))
                            ))
      )
